//
//  ViewController.swift
//  G63L5
//
//  Created by Alexandr Sopilnyak on 16.06.2018.
//  Copyright © 2018 Alexandr Sopilnyak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        playWithPhones()
        
        print("123")
        print(123)
        print("123")
        
    }

    func playWithPhones() {
        let myPhone = Phone(screen: 5.5, memory: 32)
        print(myPhone.photosCount)
        print(myPhone.description)
        
        let myPhone2 = Phone(screen: 4.0, memory: 16)
        myPhone2.makePhoto()
        myPhone2.makePhoto()
        myPhone2.makePhoto()
        print(myPhone2.description)
    }
    
    
}

